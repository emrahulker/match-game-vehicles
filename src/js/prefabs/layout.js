import Vehicle from "./vehicles"
import Ghost from "./ghosts"
export default class Layout extends Phaser.Group {

    constructor({game, session, vehicles, ghosts}) {
        super(game);
        this.game = game;
        this.session=session;
        this.vehicles = vehicles;
        this.ghosts = ghosts;
        this.GhostDict = {};
        this.init();
    }

    init() {

        // buraya vehicles ve ghosts dışında iki array yaratıyorum ki isim hallerine değil obje hallerine erişebileyim
        this.vehicleObjects = [];
        this.ghostObjects = [];
        this.ghostArray = [];

        // burada ghost objeleri dict e çeviriyorum vehicle da nesneye direct ulaşabilmek için
        this.GhostDict = {};
        for(let i=0;i<this.ghosts.length;i++){
            this.GhostDict[this.ghosts[i].id] = this.ghosts[i].image;
        }

        this.hiddenPos = {
            x: otsimo.kv.game.hiddenPos.x * this.game.width,
            y: otsimo.kv.game.hiddenPos.y * this.game.height
        };

        this.visiblePos = {
            x: otsimo.kv.game.visiblePos.x * this.game.width,
            y: otsimo.kv.game.visiblePos.y * this.game.height
        };

        this.x = this.hiddenPos.x;
        this.y = this.hiddenPos.y;

        // add vehicles to layout
        for (let i = 0; i < this.vehicles.length; i++) {


            let oVehicle = new Vehicle({
                game: this.game,
                session:this.session,
                x: ( 0.2 * this.game.width) ,
                y: (0.25 + (i * 0.3)) * this.game.height,
                vehicle: this.vehicles[i]
            });

            oVehicle.events.onDragStart.add(oVehicle.onDragStart, oVehicle);
            oVehicle.events.onDragUpdate.add(oVehicle.dragUpdate, oVehicle);
            oVehicle.events.onDragStop.add(oVehicle.onDragStop, oVehicle);

            this.add(oVehicle);
            this.vehicleObjects.push(oVehicle);
            this.ghostArray.push(this.GhostDict[this.vehicles[i].ghost_id]);

            if(otsimo.kv.game.difficulty[1].item_count==(i+1)){
                break;
            }

        }


        // add ghosts to layout
        this.ghostArray = this.shuffle(this.ghostArray);
        for (let i = 0; i < this.ghostArray.length; i++) {

            let oGhost = new Ghost({
                game: this.game,
                x: (0.7 * this.game.width),
                y: (0.25 + (i * 0.3)) * this.game.height,
                item: this.ghostArray[i]
            });
            this.add(oGhost);
            this.ghostObjects.push(oGhost);
        }


    }



    shuffle(array) {
      let currentIndex = array.length, temporaryValue, randomIndex;

      // While there remain elements to shuffle...
      while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    }

    layoutMoveTo(position) {
        let t = this.game.add.tween(this).to({ x: position.x, y: position.y }, 400, Phaser.Easing.Exponential.Out);
        t.start();
    }

}
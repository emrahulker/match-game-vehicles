export default class Ghost extends Phaser.Sprite {

    constructor({game, x, y, item}) {
        // item'ları verirken zaten image ismi olarak vermişsin
        // ayrıca item.image demen undefined yapıyo image'ı
        super(game, x, y, item);
        this.game = game;
        this.item = item;
        this.anchor.set(0.5, 0.5);
        this.x = x;
        this.y = y;
        this.fixedPosition_ = { x: x, y: y };
        this.onDragUpdate = new Phaser.Signal();
        this.full = false;
        this.onDrag = false;
        this.type = undefined;
    }

    playSound() {
        if (this.item.tts === true) {
            otsimo.tts.speak(this.item.text);
        } else {
            this.game.sound.play(this.item.audio);
        }
    }

    enableDrag() {
        this.inputEnabled = true;
        this.input.enableDrag(false, true);
    }

    onDragStart() {
        this.onDrag = true;
        this.defaultScaleX = this.scale.x;
        this.defaultScaleY = this.scale.y;

        let ns = this.scale.x * 1.1;
        otsimo.game.add.tween(this.scale).to({ x: ns, y: ns }, 100, Phaser.Easing.Back.Out, true);
    }

    onDragStop() {
        this.onDrag = false;
        this.lastDragPointer = null;
        otsimo.game.add.tween(this.scale).to({ x: this.defaultScaleX, y: this.defaultScaleY }, 100, Phaser.Easing.Back.Out, true);
        if (!this.doNotMoveAfterDrag) {
            otsimo.game.add.tween(this)
                .to({ x: this.fixedPosition_.x, y: this.fixedPosition_.y }, otsimo.kv.game.onDragStopDuration, Phaser.Easing.Back.Out, true);
        }
    }

    dragUpdate(sprite, pointer, dragX, dragY, snapPoint) {
        this.onDrag = true;
        this.lastDragPointer = pointer;
        this.onDragUpdate.dispatch(this);
    }

    stopDrag() {
        this.onDrag = false;
        if (this.lastDragPointer) {
            this.doNotMoveAfterDrag = true;
            this.input.stopDrag(this.lastDragPointer);
            this.lastDragPointer = null;
            this.doNotMoveAfterDrag = false;
        }
    }

    stopAndDisableDrag() {
        this.onDrag = false;
        this.doNotMoveAfterDrag = true;
        this.inputEnabled = false;
        this.input.disableDrag();
        this.stopDrag();
    }


}
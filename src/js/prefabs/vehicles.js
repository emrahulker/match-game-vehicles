export default class Vehicle extends Phaser.Sprite {

    constructor({game,session, x, y, vehicle}) {
        // item'ları verirken zaten image ismi olarak vermişsin
        // ayrıca item.image demen undefined yapıyo image'ı
        super(game, x, y, vehicle.image);
        this.game = game;
        this.session=session;
        this.item = vehicle.image;
        this.vehicle = vehicle;
        this.anchor.set(0.5, 0.5);
        this.x = x;
        this.y = y;
        this.z = 10;
        this.fixedPosition_ = { x: x, y: y };
        this.onDragUpdate = new Phaser.Signal();
        this.full = false;
        this.onDrag = false;
        this.type = undefined;
        this.enableDrag();
        this.score = otsimo.kv.game.stepScore;
        this.isMatched = false;
        //

    }

    playSound() {
        if (this.item.tts === true) {
            otsimo.tts.speak(this.item.text);
        } else {
            this.game.sound.play(this.item.audio);
        }
    }

    enableDrag() {

        this.inputEnabled = true;
        this.input.enableDrag(false, true);
    }

    onDragStart() {
        this.onDrag = true;
        this.defaultScaleX = this.scale.x;
        this.defaultScaleY = this.scale.y;

        let ns = this.scale.x * 1.1;
        otsimo.game.add.tween(this.scale).to({ x: ns, y: ns }, 100, Phaser.Easing.Back.Out, true);
    }

    onDragStop() {
        this.onDrag = false;
        this.lastDragPointer = null;
        otsimo.game.add.tween(this.scale).to({ x: this.defaultScaleX, y: this.defaultScaleY }, 100, Phaser.Easing.Back.Out, true);

    }

    collides(vehicle, preciseCalc = false, precisionRate = 0) {



        for(let j=0;j< this.parent.children.length; j++){

            let child = this.parent.children[j];
            if(child.item.split('_').length == 2){
                if(child.item.split('_')[0] == 'ghost' && child.item.split('_')[1] == vehicle.item.split('_')[1]){
                    let ghost = child;

                    let intsect = Phaser.Rectangle.intersection(vehicle, ghost);
                    let intsectRateX = intsect.width / ghost.width;
                    let intsectRateY = intsect.height / ghost.height;

                    if (intsectRateX > precisionRate && intsectRateY > precisionRate) {
                        this.x=ghost.x;
                        this.y=ghost.y;
                        this.stopAndDisableDrag();
                        ghost.destroy();
                        this.isMatched=true;
                        return true
                    }
                }
            }

        }
        vehicle.score -=1;
        if(vehicle.score < 0){
            vehicle.score = 0;
        }

        console.log(vehicle.item);
        console.log(vehicle.score);
        return false;
    }

    dragUpdate(sprite, pointer, dragX, dragY, snapPoint) {
        this.onDrag = true;
        this.lastDragPointer = pointer;
        this.onDragUpdate.dispatch(this);
    }

    stopAndDisableDrag() {
        this.onDrag = false;
        this.doNotMoveAfterDrag = true;
        this.inputEnabled = false;
        this.input.disableDrag();
    }

}
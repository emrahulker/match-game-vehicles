import Layout from "./prefabs/layout"
import Hint from "./prefabs/hint"

export default class Scene {

    constructor({game, session}) {
        this.game = game;
        this.session = session;
        this.hint_num = 0;
    }


    init() {



        let currentState = otsimo.game.state.getCurrentState().key;
        if (currentState != "Play") {
            return;
        }

        let layout = new Layout({
            game: this.game,
            session: this.session,
            vehicles: otsimo.kv.vehicles,
            ghosts: otsimo.kv.ghosts
        });
        this.layout = layout;

        for (let i = 0; i < this.layout.vehicleObjects.length; i++) {
            this.layout.vehicleObjects[i].events.onDragStop.add(this.onDragEnd, this, this.layout.vehicleObjects[i]);
            this.layout.vehicleObjects[i].onDragUpdate.add(this.onDrag, this, this.layout.vehicleObjects[i]);
        }
        this.layout.layoutMoveTo(this.layout.visiblePos);
        this.createHint()

    }

    createHint(){

        if(!this.layout.vehicleObjects[this.hint_num].isMatched) {

            if (typeof(this.hint) != 'undefined') {
                this.hint.kill();
                this.hint.removeTimer();
            }

            let hint = new Hint({
                game: this.game,
                vehicle: this.layout.vehicleObjects[this.hint_num],
                ghost: this.getGhostObject(this.layout.vehicleObjects[this.hint_num])
            });

            this.hint_num += 1;
            if (this.hint_num == this.layout.vehicleObjects.length) {
                this.hint_num = 0;
            }

            this.hint = hint;
            this.hint.call(0);
            otsimo.game.time.events.add((otsimo.settings.hint_duration * 1000), this.createHint, this);

        }else{
            this.hint_num += 1;
            if (this.hint_num == this.layout.vehicleObjects.length) {
                this.hint_num = 0;
            }
            this.createHint();
        }

    }

    getGhostObject(vehicle){

        let ghostName = this.layout.GhostDict[vehicle.vehicle.ghost_id];
        for(let j=0;j< this.layout.ghostObjects.length; j++){
           if(this.layout.ghostObjects[j].item == ghostName){
               return this.layout.ghostObjects[j];
           }
        }
    }

    onDrag(){
        this.hint.kill();
        this.hint.removeTimer();
    }

    onDragEnd(vehicle) {

        let isCollided = vehicle.collides(vehicle);
        if (!isCollided) {
            otsimo.game.add.tween(vehicle).to({ x: vehicle.fixedPosition_.x, y: vehicle.fixedPosition_.y }, otsimo.kv.game.onDragStopDuration, Phaser.Easing.Back.Out, true);
            this.session.wrongInput();
        } else {
            this.session.correctInput(vehicle);
            this.session.startStep(vehicle);
        }

        if (this.session.completed_task == otsimo.kv.game.difficulty[1].item_count) {
            this.session.end();
            this.startOverState(0);
        }else{
            //this.createHint();
        }

    }


    startOverState(delay) {
        setTimeout(() => {
            otsimo.game.state.start("Over")
        }, delay);

    }





}

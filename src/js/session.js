import Balloon from './prefabs/balloon'

export default class Session {
    constructor({state}) {

        this.startTime = new Date()
        this.state = state
        this.wrongAnswerTotal = 0
        this.wrongAnswerStep = 0
        this.completed_task=0
        this.totalScore = 0;
        this.stepScore = otsimo.kv.game.stepScore;

    }

    end() {
        const fin = Date.now();
        const delta = fin - this.startTime;

        const payload = {
          score: this.score,
          duration: delta,
          wrongAnswerTotal: this.wrongAnswerTotal,
          correctAnswerTotal: this.correctAnswerTotal,
          id: this.id,
          difficulty: otsimo.settings.difficulty,
          difficulty_items: this._itemAmount,
          ab_test: this.ab,
        }
        otsimo.customevent('game:session:end', payload)
    }

    startStep(vehicle) {
        this.wrongAnswerStep = 0;
        this.stepScore = vehicle.score;
    }

    wrongInput(item, amount) {
        this.wrongAnswerStep += 1
        this.wrongAnswerTotal += 1
    }

    correctInput(answerItem){
        this.totalScore += answerItem.score;
        this.completed_task += 1;
    }

    debug(game) {
        game.debug.text("step score: " + this.stepScore, 2, 28, "#00ff00");
        game.debug.text("total score: " + this.totalScore, 2, 48, "#00ff00");
        game.debug.text("wrongAnswerTotal: " + this.wrongAnswerTotal, 2, 62, "#00ff00");
        game.debug.text("wrongAnswerStep: " + this.wrongAnswerStep, 2, 84, "#00ff00");
    }
}
import Session from '../session'
import Balloon from '../prefabs/balloon'
import Scene from '../scene'

export default class Play extends Phaser.State {

    create() {

        let session = new Session({ state: this });
        this.session = session;

        let scene = new Scene({game:this.game,session:this.session});
        this.game.add.button(25, 25, 'back', this.backAction, this);

        if (otsimo.kv.background_image) {
            let back = this.game.add.image(this.game.world.centerX, this.game.world.centerY, otsimo.kv.background_image)
            back.anchor.set(0.5, 0.5);
        }

        scene.init();

    }

    backAction(button) {
        this.game.state.start('Home');
    }

    render() {
        if (otsimo.debug) {
            this.game.debug.text(this.game.time.fps || '--', 2, 14, "#00ff00");
            this.session.debug(this.game);
        }
    }

    sceneEnded() {
        this.session.end();
        this.game.state.start('Over');
    }
}
